验证 AKS 容器服务 
SOFABoot

```$sh
FROM reg-cnsh.cloud.alipay.com/library/sofaboot-onbuild:2.0_java8_p1 AS BUILDER
# Executing 3 build triggers
#   ARG APPNAME
#   COPY . /home/admin/app
#   RUN chown -R admin:admin /home/admin/app
MAINTAINER hongwen.chw <hongwen.chw@antfin.com>

RUN /home/admin/buildscripts/sofaboot-build -sk=true -sfp=/home/admin/app/mvn/settings.xml


FROM reg-cnsh.cloud.alipay.com/library/sofaboot-runtime:2.0_java8_monitor_fix1
COPY --from=BUILDER /home/admin/release/run/*.jar /home/admin/release/run/

CMD /bin/bash -c /home/admin/scripts/sofaboot-entry.sh
```