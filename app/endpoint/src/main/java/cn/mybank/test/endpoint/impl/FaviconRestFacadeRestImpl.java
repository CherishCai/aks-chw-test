
package cn.mybank.test.endpoint.impl;

import cn.mybank.test.endpoint.exception.CommonException;
import cn.mybank.test.endpoint.facade.FaviconRestFacade;

/**
 * rest service implementation for the favicon
 * <p>
 * <p/>
 * Created by luoguimu on 16/11/18.
 */
public class FaviconRestFacadeRestImpl implements FaviconRestFacade {

    public String faviconIco() throws CommonException{
        return "";
    }

}
